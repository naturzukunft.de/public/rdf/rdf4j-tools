package de.naturzukunft.rdf4j.ommapper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Iri(ReferencedPojo.NAMESPACE)
public class ReferencedPojo extends BaseObject {
	public static final String NAMESPACE = "http://example.com/referencedPojo/";
	
	@Iri(NAMESPACE + "String")
	@NonNull
	private String someAttribte;

}
