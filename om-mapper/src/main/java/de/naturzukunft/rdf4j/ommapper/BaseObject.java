package de.naturzukunft.rdf4j.ommapper;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
public abstract class BaseObject {
	@Iri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
	@NonNull
	private Set<IRI> type;
	
	@Iri("http://www.w3.org/2002/07/owl#sameAs")
	private Set<IRI> sameAs;
	
	@NonNull
	private IRI subject;
}
