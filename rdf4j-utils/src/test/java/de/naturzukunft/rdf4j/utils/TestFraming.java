package de.naturzukunft.rdf4j.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.github.jsonldjava.core.JsonLdError;

public class TestFraming {

	@Test
	public void test() throws RDFParseException, JsonLdError, UnsupportedRDFormatException, IOException {
		Framing framing = new Framing();
		Model model = Rio.parse(new ClassPathResource("Framing_Test_Teikei_Trudering_sample.ttl").getInputStream(), RDFFormat.TURTLE);
		String converted = framing.convert(model);
		
		String expected = new String(
				new ClassPathResource("Framing_Test_Teikei_Trudering_sample.jsonld").getInputStream().readAllBytes(),
				StandardCharsets.UTF_8);
		assertEquals(expected.trim(), converted.trim());
	}
	
}
