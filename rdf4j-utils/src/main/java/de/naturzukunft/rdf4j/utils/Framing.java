package de.naturzukunft.rdf4j.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Map;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonParseException;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;

public class Framing {
	public String convert(Model inModel) throws IOException, JsonLdError {

		// set JSON-LD options
		JsonLdOptions opts = new JsonLdOptions();
		opts.setOmitDefault(Boolean.TRUE);
		opts.setProcessingMode(JsonLdOptions.JSON_LD_1_1);
	
		// convert the JSON-LD with frame to a more human readable shape		
		Map<String,Object> res = JsonLdProcessor.frame(toJsonLdProcessorInput(inModel), getActivityStreamsFrame(), opts); 

		StringWriter writer = new StringWriter();
		JsonUtils.writePrettyPrint(writer, res);
		return writer.toString();
	}

	private Object toJsonLdProcessorInput(Model inMoodel) throws JsonParseException, IOException {
		StringWriter stringWriter = new StringWriter();
		Rio.write(inMoodel, stringWriter, RDFFormat.JSONLD);
		String string = stringWriter.toString();
		return JsonUtils.fromString(string);
	}

	private Object getActivityStreamsFrame() throws IOException {		
		ClassPathResource asFrame = new ClassPathResource("activityStreams-context.jsonld");// available via dependency: de.naturzukunft.rdf4j:vocabulary
		Object frame = JsonUtils.fromReader(new InputStreamReader(asFrame.getInputStream()));
		return frame;
	}
}
