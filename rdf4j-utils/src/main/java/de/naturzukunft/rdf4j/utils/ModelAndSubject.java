package de.naturzukunft.rdf4j.utils;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Often you need a model and the subject of the 'root' object in that model.
 * Yes, i know 'a graph did not have a root object'. But e.G. in the context of
 * activityPub a model contains an activity, it's object and objects of that
 * object. And then you have to know the subject of that activity. Even if a
 * object in that graph depends on that activity!
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelAndSubject {
	
	@NonNull
	private IRI subject;
	
	@NonNull
	private Model model;
	
	@Override
	public String toString() {
		String modelAsString = ModelLogger.toString(model, RDFFormat.TRIG);		
		return "ModelAndSubject [subject=" + subject + ", model=\n" + modelAsString + "]";
	}
}
