package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

import org.apache.commons.rdf.api.IRI;

public interface Vocabulary {
	IRI getNamespace();
}
