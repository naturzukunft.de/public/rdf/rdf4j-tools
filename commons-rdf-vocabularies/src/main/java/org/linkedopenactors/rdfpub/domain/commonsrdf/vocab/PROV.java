package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

public class PROV {

	public static final String NS = "http://www.w3.org/ns/prov#";
	
	  public final static String Activity = NS + "Activity";
	  public final static String Entity = NS + "Entity";
	  public final static String wasRevisionOf = NS + "wasRevisionOf";
	  public final static String wasDerivedFrom = NS + "wasDerivedFrom";
	  public final static String wasGeneratedBy = NS + "wasGeneratedBy";
	  public final static String wasAttributedTo = NS + "wasAttributedTo";
	  public final static String wasAssociatedWith = NS + "wasAssociatedWith";
	  public final static String wasInformedBy = NS + "wasInformedBy";
}

