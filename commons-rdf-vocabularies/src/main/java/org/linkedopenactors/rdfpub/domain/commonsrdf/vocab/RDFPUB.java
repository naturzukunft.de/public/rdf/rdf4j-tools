package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

import org.apache.commons.rdf.api.IRI;

public interface RDFPUB extends Vocabulary {

	public static final String NS = "https://schema.rdf-pub.org/";

	/**
	 * rdf-pub commitId
	 * <p>
	 * {@code http://rdf-pub.org#commitId}
	 * <p>
	 * The git commitId from which this instance was built.
	 * @see <a href="http://rdf-pub.org#commitId">commitId</a>
	 */
	public final static String commitId = NS + "commitId";
	
	public final static String oauth2IssuerUserId = NS + "oauth2IssuerUserId";
	
	public final static String  version = NS + "version";

	IRI commitId();
	
	IRI oauth2IssuerUserId();
	
	IRI version();
}
