package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

import org.apache.commons.rdf.api.IRI;

public interface LDP extends Vocabulary {

	public static final String NS = "http://www.w3.org/ns/ldp#";
	public final static String inbox = NS + "inbox";
	public IRI inbox();
}
