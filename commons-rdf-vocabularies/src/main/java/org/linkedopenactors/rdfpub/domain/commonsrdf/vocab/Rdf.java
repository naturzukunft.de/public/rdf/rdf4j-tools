package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

import org.apache.commons.rdf.api.IRI;

public interface Rdf extends Vocabulary {
	
	public static final String NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static String type = NS + "type";
	public final static String Bag = NS + "Bag";
	public final static String Property = NS + "Property";
	
	IRI type();
	IRI Bag();
	IRI Property();
}
