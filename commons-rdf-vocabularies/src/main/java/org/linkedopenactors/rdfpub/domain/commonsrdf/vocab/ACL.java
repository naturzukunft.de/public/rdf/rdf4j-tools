package org.linkedopenactors.rdfpub.domain.commonsrdf.vocab;

public class ACL {
	public static final String NS = "http://www.w3.org/ns/auth/acl#";
	public final static String Authorization = NS + "Authorization";
	public final static String agent = NS + "agent";	
	public final static String accessTo = NS + "accessTo";
	public final static String mode = NS + "mode";
	public final static String Read = NS + "Read";
	public final static String Write = NS + "Write";
	public final static String Append = NS + "Append";
    }
