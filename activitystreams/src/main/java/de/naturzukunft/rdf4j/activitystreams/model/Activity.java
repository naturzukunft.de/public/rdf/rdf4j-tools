package de.naturzukunft.rdf4j.activitystreams.model;

import java.util.Set;

import org.eclipse.rdf4j.model.IRI;

import de.naturzukunft.rdf4j.ommapper.Iri;
import de.naturzukunft.rdf4j.vocabulary.AS;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Iri(AS.NAMESPACE)
public class Activity extends AsObject {

	/**
	 * Describes one or more entities that either performed or are expected to
	 * perform the activity. Any single activity can have multiple actors. The
	 * actorMAY be specified using an indirect Link.
	 * @See https://www.w3.org/ns/activitystreams#actor
	 */
	@Iri(AS.ACTOR_STRING)
	private Set<IRI> actor;

	/**
	 * Describes an object of any kind. The Object type serves as the base type for
	 * most of the other kinds of objects defined in the Activity Vocabulary,
	 * including other Core types such as Activity, IntransitiveActivity, Collection
	 * and OrderedCollection.
	 * @See https://www.w3.org/ns/activitystreams#object
	 */
	@Iri(AS.OBJECT_STRING)
	private IRI object;
	
	/**
	 * Describes the indirect object, or target, of the activity. The precise
	 * meaning of the target is largely dependent on the type of action being
	 * described but will often be the object of the English preposition "to". For
	 * instance, in the activity "John added a movie to his wishlist", the target of
	 * the activity is John's wishlist. An activity can have more than one target.
	 * @See https://www.w3.org/ns/activitystreams#target
	 */
	@Iri(AS.TARGET_STRING)
	private Set<IRI> target;
	
	/**
	 * Describes the result of the activity. For instance, if a particular action
	 * results in the creation of a new resource, the result property can be used to
	 * describe that new resource.
	 * @See https://www.w3.org/ns/activitystreams#result
	 */
	@Iri(AS.RESULT_STRING)
	private Set<IRI> result;
	
	/**
	 * Describes an indirect object of the activity from which the activity is
	 * directed. The precise meaning of the origin is the object of the English
	 * preposition "from". For instance, in the activity "John moved an item to List
	 * B from List A", the origin of the activity is "List A".
	 * @See https://www.w3.org/ns/activitystreams#origin
	 */
	@Iri(AS.ORIGIN_STRING)
	private Set<IRI> origin;
		
	/**
	 * Identifies one or more objects used (or to be used) in the completion of an Activity.
	 * @See https://www.w3.org/ns/activitystreams#instrument 
	 */
	@Iri(AS.INSTRUMENT_STRING)
	private Set<IRI> instrument;
}
