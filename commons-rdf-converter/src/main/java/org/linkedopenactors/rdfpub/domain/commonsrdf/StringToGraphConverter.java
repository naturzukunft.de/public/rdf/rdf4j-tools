package org.linkedopenactors.rdfpub.domain.commonsrdf;

import java.util.Map;
import java.util.Set;

import org.apache.commons.rdf.api.BlankNodeOrIRI;
import org.apache.commons.rdf.api.Graph;

public interface StringToGraphConverter {
	Graph convert(RdfFormat rdfFormat, String graphAsString);
	Map<BlankNodeOrIRI, Graph> convert(RdfFormat rdfFormat, Set<BlankNodeOrIRI> subjects,  String graphAsString);
}
