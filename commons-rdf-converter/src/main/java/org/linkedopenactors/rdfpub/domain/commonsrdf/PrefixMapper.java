package org.linkedopenactors.rdfpub.domain.commonsrdf;

public interface PrefixMapper {

	String getInternaleIriPrefix();

	String getExternalIriPrefix();

}
