package org.linkedopenactors.rdfpub.domain.commonsrdf;

public enum RdfFormat {
	TURTLE, TRIG, JSONLD, RDFXML;
}
