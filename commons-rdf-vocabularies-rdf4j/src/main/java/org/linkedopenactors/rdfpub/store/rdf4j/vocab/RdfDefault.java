package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.Rdf;

public class RdfDefault implements Rdf {

	private RDF4J rdf;

	public RdfDefault() {
		rdf = new RDF4J();
	}

	@Override
	public IRI getNamespace() {
		return rdf.createIRI(Rdf.NS);
	}

	@Override
	public IRI type() {
		return rdf.createIRI(Rdf.type);
	}

	@Override
	public IRI Bag() {
		return rdf.createIRI(Rdf.Bag);
	}

	@Override
	public IRI Property() {
		return rdf.createIRI(Rdf.Property);
	}
}
