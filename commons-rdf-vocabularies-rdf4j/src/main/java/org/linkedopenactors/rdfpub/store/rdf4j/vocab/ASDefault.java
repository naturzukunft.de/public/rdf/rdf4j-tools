package org.linkedopenactors.rdfpub.store.rdf4j.vocab;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdfrdf4j.RDF4J;
import org.linkedopenactors.rdfpub.domain.commonsrdf.vocab.AS;
import org.springframework.stereotype.Component;

@Component
public class ASDefault implements AS {

	private RDF4J rdf;

	public ASDefault() {
		rdf = new RDF4J();
	}

	@Override
	public IRI getNamespace() {
		return rdf.createIRI(AS.NS);
	}

	@Override
	public IRI Object() {
		return rdf.createIRI(AS.Object);
	}

	@Override
	public IRI Link() {
		return rdf.createIRI(AS.Link);
	}

	@Override
	public IRI Activity() {
		return rdf.createIRI(AS.Activity);
	}

	@Override
	public IRI IntransitiveActivity() {
		return rdf.createIRI(AS.IntransitiveActivity);
	}

	@Override
	public IRI Collection() {
		return rdf.createIRI(AS.Collection);
	}

	@Override
	public IRI OrderedCollection() {
		return rdf.createIRI(AS.OrderedCollection);
	}

	@Override
	public IRI CollectionPage() {
		return rdf.createIRI(AS.CollectionPage);
	}

	@Override
	public IRI OrderedCollectionPage() {
		return rdf.createIRI(AS.OrderedCollectionPage);
	}

	@Override
	public IRI Accept() {
		return rdf.createIRI(AS.Accept);
	}

	@Override
	public IRI TentativeAccept() {
		return rdf.createIRI(AS.TentativeAccept);
	}

	@Override
	public IRI Add() {
		return rdf.createIRI(AS.Add);
	}

	@Override
	public IRI Arrive() {
		return rdf.createIRI(AS.Arrive);
	}

	@Override
	public IRI Create() {
		return rdf.createIRI(AS.Create);
	}

	@Override
	public IRI Delete() {
		return rdf.createIRI(AS.Delete);
	}

	@Override
	public IRI Follow() {
		return rdf.createIRI(AS.Follow);
	}

	@Override
	public IRI Ignore() {
		return rdf.createIRI(AS.Ignore);
	}

	@Override
	public IRI Join() {
		return rdf.createIRI(AS.Join);
	}

	@Override
	public IRI Leave() {
		return rdf.createIRI(AS.Leave);
	}

	@Override
	public IRI Like() {
		return rdf.createIRI(AS.Like);
	}

	@Override
	public IRI Offer() {
		return rdf.createIRI(AS.Offer);
	}

	@Override
	public IRI Invite() {
		return rdf.createIRI(AS.Invite);
	}

	@Override
	public IRI Reject() {
		return rdf.createIRI(AS.Reject);
	}

	@Override
	public IRI TentativeReject() {
		return rdf.createIRI(AS.TentativeReject);
	}

	@Override
	public IRI Remove() {
		return rdf.createIRI(AS.Remove);
	}

	@Override
	public IRI Undo() {
		return rdf.createIRI(AS.Undo);
	}

	@Override
	public IRI Update() {
		return rdf.createIRI(AS.Update);
	}

	@Override
	public IRI View() {
		return rdf.createIRI(AS.View);
	}

	@Override
	public IRI Listen() {
		return rdf.createIRI(AS.Listen);
	}

	@Override
	public IRI Read() {
		return rdf.createIRI(AS.Read);
	}

	@Override
	public IRI Move() {
		return rdf.createIRI(AS.Move);
	}

	@Override
	public IRI Travel() {
		return rdf.createIRI(AS.Travel);
	}

	@Override
	public IRI Announce() {
		return rdf.createIRI(AS.Announce);
	}

	@Override
	public IRI Block() {
		return rdf.createIRI(AS.Block);
	}

	@Override
	public IRI Flag() {
		return rdf.createIRI(AS.Flag);
	}

	@Override
	public IRI Dislike() {
		return rdf.createIRI(AS.Dislike);
	}

	@Override
	public IRI Question() {
		return rdf.createIRI(AS.Question);
	}

	@Override
	public IRI Application() {
		return rdf.createIRI(AS.Application);
	}

	@Override
	public IRI Group() {
		return rdf.createIRI(AS.Group);
	}

	@Override
	public IRI Organization() {
		return rdf.createIRI(AS.Organization);
	}

	@Override
	public IRI Person() {
		return rdf.createIRI(AS.Person);
	}

	@Override
	public IRI Service() {
		return rdf.createIRI(AS.Service);
	}

	@Override
	public IRI Relationship() {
		return rdf.createIRI(AS.Relationship);
	}

	@Override
	public IRI Article() {
		return rdf.createIRI(AS.Article);
	}

	@Override
	public IRI Document() {
		return rdf.createIRI(AS.Document);
	}

	@Override
	public IRI Audio() {
		return rdf.createIRI(AS.Audio);
	}

	@Override
	public IRI Image() {
		return rdf.createIRI(AS.Image);
	}

	@Override
	public IRI Video() {
		return rdf.createIRI(AS.Video);
	}

	@Override
	public IRI Note() {
		return rdf.createIRI(AS.Note);
	}

	@Override
	public IRI Page() {
		return rdf.createIRI(AS.Page);
	}

	@Override
	public IRI Event() {
		return rdf.createIRI(AS.Event);
	}

	@Override
	public IRI endpoints() {
		return rdf.createIRI(AS.endpoints);
	}

	@Override
	public IRI Place() {
		return rdf.createIRI(AS.Place);
	}

	@Override
	public IRI Mention() {
		return rdf.createIRI(AS.Mention);
	}

	@Override
	public IRI Profile() {
		return rdf.createIRI(AS.Profile);
	}

	@Override
	public IRI Tombstone() {
		return rdf.createIRI(AS.Tombstone);
	}

	@Override
	public IRI actor() {
		return rdf.createIRI(AS.actor);
	}

	@Override
	public IRI attachment() {
		return rdf.createIRI(AS.attachment);
	}

	@Override
	public IRI attributedTo() {
		return rdf.createIRI(AS.attributedTo);
	}

	@Override
	public IRI audience() {
		return rdf.createIRI(AS.audience);
	}

	@Override
	public IRI bcc() {
		return rdf.createIRI(AS.bcc);
	}

	@Override
	public IRI bto() {
		return rdf.createIRI(AS.bto);
	}

	@Override
	public IRI cc() {
		return rdf.createIRI(AS.cc);
	}

	@Override
	public IRI context() {
		return rdf.createIRI(AS.context);
	}

	@Override
	public IRI current() {
		return rdf.createIRI(AS.current);
	}

	@Override
	public IRI first() {
		return rdf.createIRI(AS.first);
	}

	@Override
	public IRI generator() {
		return rdf.createIRI(AS.generator);
	}

	@Override
	public IRI icon() {
		return rdf.createIRI(AS.icon);
	}

	@Override
	public IRI image() {
		return rdf.createIRI(AS.image);
	}

	@Override
	public IRI inReplyTo() {
		return rdf.createIRI(AS.inReplyTo);
	}

	@Override
	public IRI instrument() {
		return rdf.createIRI(AS.instrument);
	}

	@Override
	public IRI last() {
		return rdf.createIRI(AS.last);
	}

	@Override
	public IRI location() {
		return rdf.createIRI(AS.location);
	}

	@Override
	public IRI items() {
		return rdf.createIRI(AS.items);
	}

	@Override
	public IRI oneOf() {
		return rdf.createIRI(AS.oneOf);
	}

	@Override
	public IRI anyOf() {
		return rdf.createIRI(AS.anyOf);
	}

	@Override
	public IRI closed() {
		return rdf.createIRI(AS.closed);
	}

	@Override
	public IRI origin() {
		return rdf.createIRI(AS.origin);
	}

	@Override
	public IRI next() {
		return rdf.createIRI(AS.next);
	}

	@Override
	public IRI object() {
		return rdf.createIRI(AS.object);
	}

	@Override
	public IRI prev() {
		return rdf.createIRI(AS.prev);
	}

	@Override
	public IRI preview() {
		return rdf.createIRI(AS.preview);
	}

	@Override
	public IRI result() {
		return rdf.createIRI(AS.result);
	}

	@Override
	public IRI replies() {
		return rdf.createIRI(AS.replies);
	}

	@Override
	public IRI tag() {
		return rdf.createIRI(AS.tag);
	}

	@Override
	public IRI target() {
		return rdf.createIRI(AS.target);
	}

	@Override
	public IRI to() {
		return rdf.createIRI(AS.to);
	}

	@Override
	public IRI url() {
		return rdf.createIRI(AS.url);
	}

	@Override
	public IRI accuracy() {
		return rdf.createIRI(AS.accuracy);
	}

	@Override
	public IRI altitude() {
		return rdf.createIRI(AS.altitude);
	}

	@Override
	public IRI content() {
		return rdf.createIRI(AS.content);
	}

	@Override
	public IRI name() {
		return rdf.createIRI(AS.name);
	}

	@Override
	public IRI duration() {
		return rdf.createIRI(AS.duration);
	}

	@Override
	public IRI height() {
		return rdf.createIRI(AS.height);
	}

	@Override
	public IRI href() {
		return rdf.createIRI(AS.href);
	}

	@Override
	public IRI hreflang() {
		return rdf.createIRI(AS.hreflang);
	}

	@Override
	public IRI partOf() {
		return rdf.createIRI(AS.partOf);
	}

	@Override
	public IRI latitude() {
		return rdf.createIRI(AS.latitude);
	}

	@Override
	public IRI longitude() {
		return rdf.createIRI(AS.longitude);
	}

	@Override
	public IRI mediaType() {
		return rdf.createIRI(AS.mediaType);
	}

	@Override
	public IRI endTime() {
		return rdf.createIRI(AS.endTime);
	}

	@Override
	public IRI published() {
		return rdf.createIRI(AS.published);
	}

	@Override
	public IRI Public() {
		return rdf.createIRI(AS.Public);
	}

	@Override
	public IRI preferredUsername() {
		return rdf.createIRI(AS.preferredUsername);
	}

	@Override
	public IRI startTime() {
		return rdf.createIRI(AS.startTime);
	}

	@Override
	public IRI radius() {
		return rdf.createIRI(AS.radius);
	}

	@Override
	public IRI rel() {
		return rdf.createIRI(AS.rel);
	}

	@Override
	public IRI startIndex() {
		return rdf.createIRI(AS.startIndex);
	}

	@Override
	public IRI summary() {
		return rdf.createIRI(AS.summary);
	}

	@Override
	public IRI totalItems() {
		return rdf.createIRI(AS.totalItems);
	}

	@Override
	public IRI units() {
		return rdf.createIRI(AS.units);
	}

	@Override
	public IRI updated() {
		return rdf.createIRI(AS.updated);
	}

	@Override
	public IRI width() {
		return rdf.createIRI(AS.width);
	}

	@Override
	public IRI subject() {
		return rdf.createIRI(AS.subject);
	}

	@Override
	public IRI relationship() {
		return rdf.createIRI(AS.relationship);
	}

	@Override
	public IRI describes() {
		return rdf.createIRI(AS.describes);
	}

	@Override
	public IRI formerType() {
		return rdf.createIRI(AS.formerType);
	}

	@Override
	public IRI deleted() {
		return rdf.createIRI(AS.deleted);
	}

	@Override
	public IRI outbox() {
		return rdf.createIRI(AS.outbox);
	}

	@Override
	public IRI following() {
		return rdf.createIRI(AS.following);
	}

	@Override
	public IRI followers() {
		return rdf.createIRI(AS.followers);
	}

	@Override
	public IRI liked() {
		return rdf.createIRI(AS.liked);
	}

	@Override
	public IRI oauthAuthorizationEndpoint() {
		return rdf.createIRI(AS.oauthAuthorizationEndpoint);
	}

	@Override
	public IRI oauthTokenEndpoint() {
		return rdf.createIRI(AS.oauthTokenEndpoint);
	}
}
